from numpy import sqrt, pi
HBAR = 1.05E-34
h = 6.62607004E-34 #Planck constant
BOHR_TO_METERS = 5.29E-11
D_to_sqrt_J_m3 = 3.1623E-25 
au_to_kg = 1.66E-27
kHz_to_Hz = 1000
GHz_to_Hz = 1E9
class PhysicalSystem:
    def __init__(self, m1_au, m2_au, B_ni_GHz, d_Debye, a_Bohr, ho_frequency_kHz, ho_perp_frequency_kHz, a_aHO = None):
        self.m1_au = m1_au
        self.m2_au = m2_au
        self.B_omega_Hz = B_ni_GHz * 2*pi * GHz_to_Hz
        self.d_Debye = d_Debye
        
        self.ho_frequency_Hz = ho_frequency_kHz * kHz_to_Hz
        self.ho_omega_Hz = 2*pi*self.ho_frequency_Hz
        self.ho_perp_frequency_Hz = ho_perp_frequency_kHz * kHz_to_Hz
        self.eta = (self.ho_perp_frequency_Hz/self.ho_frequency_Hz)**2 
        # the values in SI
        self.mu_kg = (self.m1_au + self.m2_au)/2 * au_to_kg
        self.a_ho_m = sqrt(HBAR/(self.mu_kg * self.ho_omega_Hz))
        self.a_dd_m = 2 * self.mu_kg * self.d_Debye**2 * D_to_sqrt_J_m3**2 / HBAR**2
        self.E_ho_J =  h * self.ho_frequency_Hz
        self.E_perp_J =  h * self.ho_perp_frequency_Hz
        self.B_J = h * B_ni_GHz * GHz_to_Hz  
        self.E_dd_J = self.d_Debye**2 * D_to_sqrt_J_m3**2 /self.a_ho_m**3 
        if a_aHO is None: 
            self.a_m = a_Bohr * BOHR_TO_METERS
        elif a_Bohr is None:
            self.a_m =  a_aHO * self.a_ho_m
        self.a_aho = self.a_m/self.a_ho_m

    @property
    def units_convert(self):
        return {
            "E_ho" : {
                "E_ho" : 1,
                "B" : self.E_ho_J/self.B_J, 
                "E_perp" : self.E_ho_J/self.E_perp_J,
                "E_dd" : self.E_ho_J/self.E_dd_J if self.d_Debye != 0 else None
            },
            "E_perp" : {
                "E_perp" : 1,
                "B" : self.E_perp_J/self.B_J,
                "E_ho" : self.E_perp_J/self.E_ho_J,
                "E_dd" : self.E_perp_J/self.E_dd_J if self.d_Debye != 0 else None
            },
            "B" : {
                "B" : 1,
                "E_ho" : self.B_J/self.E_ho_J,
                "E_perp" : self.B_J/self.E_perp_J,
                "E_dd" : self.B_J  /self.E_dd_J if self.d_Debye != 0 else None
            },
            "E_dd" : {
                "B" :  self.E_dd_J /self.B_J if self.d_Debye != 0 else 0,
                "E_ho" : self.E_dd_J/self.E_ho_J if self.d_Debye != 0 else 0,
                "E_perp" : self.E_dd_J/self.E_perp_J if self.d_Debye != 0 else 0,
                "E_dd" : 1
            }
        }
    def print_values(self):
        print("Reduced mass:", '{0:.2e}'.format(self.mu_kg), "kg") 
        print("Harmonic oscillator length unit a_HO:", '{0:.2e}'.format(self.a_ho_m), "m")
        print("Harmonic oscillator energy unit E_HO:", '{0:.2e}'.format(self.E_ho_J, "J"))
        print("eta = (omega_perp/omega)^2:", self.eta)
        print("B/E_HO:", self.units_convert["B"]["E_ho"])
        print("E_HO/B:", self.units_convert["E_ho"]["B"])
        print(f"a/a_HO: {self.a_m/self.a_ho_m:.2f}")
        print("E_dd = d^2/a_HO^3 =", self.E_dd_J,"J = ", self.E_dd_J/HBAR, "hbar")
        print("E_dd/B =", self.units_convert["E_dd"]["B"])
        print("E_dd/E_HO =", self.units_convert["E_dd"]["E_ho"])
        print("a_dd/a_HO:", '{:.2f}'.format(self.a_dd_m/self.a_ho_m))