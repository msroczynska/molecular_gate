from abc import ABC, abstractmethod, abstractproperty
from sympy.physics.wigner import wigner_3j
from sympy.physics.quantum.cg import CG
from scipy.special import eval_hermite
from math import sqrt, factorial,exp
import matplotlib.pyplot as plt
import numpy as np
import pickle
import timeit
import gmpy2

class Hamiltonian(ABC):
    """Base abstract class for all the parts of the Hamiltonian. """
    def __init__(self, basis_set):
        self.basis_set = basis_set
        self.size = len(basis_set)
        self.matrix = np.zeros((self.size, self.size))
    @abstractproperty
    def unit(self):
        """Unit in which the matrix is expressed."""
        pass
    @abstractmethod
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        pass
    def calculate(self):
        """Calculates the whole matrix. Each element is computed using abstract method matrix_element. """
        for i in range(self.size):
            for j in range(self.size):
                n_i, j1_i, m1_i, j2_i, m2_i = self.basis_set[i]
                n_j, j1_j, m1_j, j2_j, m2_j = self.basis_set[j]
                self.matrix[i, j] = self.matrix_element(n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j)


class PerpendicularOscillator(Hamiltonian):
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        """ Returns the value of matrix element equals to 1 (0 + 1/2 + 0 + 1/2) only on diagonal, otherwise 0: 
            The value is expressed in the units of E_perp (perpendicular harmonic oscillator unit).
          """
        if  n_i == n_j and j1_i == j1_j and j2_i == j2_j and m1_i == m1_j and m2_i == m2_j:
            return 1
        else:
            return 0
    @property
    def unit(self):
        return "E_perp"


class Oscillator(Hamiltonian):
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        """ Returns the value of matrix element computed as follows: 
        < n_i, j1_i, m1_i, j2_i, m2_i | -1/2 d/dz^2 + 1/2 (z)^2 |n_j, j1_j, m1_j, j2_j, m2_j> = 
            = delta(j1_i, j1_j)*delta(m1_i, m1_j)*delta(j2_i, j2_j)*delta(m2_i, m2_j) *
                *[ < n_i| -1/2 d/dz^2 + 1/2 z^2 |n_j> ] = 
            = delta(j1_i, j1_j)*delta(m1_i, m1_j)*delta(j2_i, j2_j)*delta(m2_i, m2_j) *
                *[ delta(n_i, n_j) * (n_i + 1/2 )]
            The value is expressed in the units of E_ho (harmonic oscillator unit).
          """
        value = 0
        if j1_i == j1_j and j2_i == j2_j and m1_i == m1_j and m2_i == m2_j:
            if n_i == n_j:
                value += n_i + 0.5 
        return value
    @property
    def unit(self):
        return "E_ho"
    @staticmethod
    def wavefunction(n, z_aho):
        A = np.pi**(1/4) * np.sqrt(pow(2, n) * factorial(n))
        value = 1/A* eval_hermite(n, z_aho)
        return value

class Diagonal(Hamiltonian):
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        """ Returns the value of matrix element computed as follows: 
        < n_i, j1_i, m1_i, j2_i, m2_i | 1 |n_j, j1_j, m1_j, j2_j, m2_j> = 
            = delta(j1_i, j1_j)*delta(m1_i, m1_j)*delta(j2_i, j2_j)*delta(m2_i, m2_j)*delta(n_i, n_j)
            The value is expressed in the units of E_ho (harmonic oscillator unit).
          """
        return j1_i == j1_j and j2_i == j2_j and m1_i == m1_j and m2_i == m2_j and n_i == n_j
    @property
    def unit(self):
        return "E_ho"

class Linear(Hamiltonian):
    def __init__(self, basis_set, linear_integrals_file_path):
        super().__init__(basis_set)
        self.LINEAR_INTEGRALS = np.loadtxt(linear_integrals_file_path)
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        """ Returns the value of matrix element computed as follows: 
        < n_i, j1_i, m1_i, j2_i, m2_i | z |n_j, j1_j, m1_j, j2_j, m2_j> = 
            = delta(j1_i, j1_j)*delta(m1_i, m1_j)*delta(j2_i, j2_j)*delta(m2_i, m2_j) * < n_i|z|n_j>]
            The value is expressed in the units of E_ho (harmonic oscillator unit).
          """
        value = 0
        if j1_i == j1_j and j2_i == j2_j and m1_i == m1_j and m2_i == m2_j:
            value += self.LINEAR_INTEGRALS[n_i, n_j]
        return value
    @property
    def unit(self):
        return "E_ho"

class ElectricField(Hamiltonian):
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        value = 0
        if n_i == n_j: #diagonal in oscillator states
            if j2_i == j2_j and m2_i == m2_j: # diagonal in mol. 2 states
                if abs(j1_i-j1_j) == 1 and m1_i == m1_j:
                    value += sqrt((2.0*j1_j+1.0)/(2.*j1_i+1.0))* CG(j1_j, m1_j, 1, 0, j1_i, m1_i).doit()*CG(j1_j, 0, 1, 0, j1_i, 0).doit()
            if j1_i == j1_j and m1_i == m1_j: # diagonal in mol. 1 states
                if abs(j2_i-j2_j) == 1 and m2_i == m2_j:
                    value += sqrt((2.0*j2_j+1.0)/(2.*j2_i+1.0))* CG(j2_j, m2_j, 1, 0, j2_i, m2_i).doit()*CG(j2_j, 0, 1, 0, j2_i, 0).doit()
        return value
    @property
    def unit(self):
        return "B" 

class AngularMomentumSquared(Hamiltonian):
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        """ Returns the value of matrix element computed as follows: 
        < n_i, j1_i, m1_i, j2_i, m2_i | J1^2 + J2^2 |n_j, j1_j, m1_j, j2_j, m2_j> = 
            = delta(n_i, n_j) *(<j1_i, m1_i| J1^2 |j1_j, m1_j> + <j2_i, m2_i| J2^2|j2_j, m2_j>) =
            = delta(n_i, n_j) *[delta(j1_i, j1_j)*delta(m1_i, m1_j)* j1_i*(j1_i+1) + 
                                + delta(j2_i, j2_j)*delta(m2_i, m2_j)* j2_i*(j2_i+1)]
            The value is expressed in the units of B (rotational constant).
          """
        value = 0
        if n_i == n_j:
            if j1_i == j1_j and m1_i == m1_j and j2_i == j2_j and m2_i == m2_j: 
                value += j1_i * (j1_i + 1)
                value += j2_i * (j2_i + 1) 
        return value
    @property
    def unit(self):
        return "B"
        

class DipoleDipole(Hamiltonian):
    def __init__(self, basis_set, eta, vdd_integrals_file_path, c_values_file_path, a = 0, theta = 0):
        super().__init__(basis_set)
        self.VDD_INTEGRALS = np.loadtxt(vdd_integrals_file_path)
        try:
            self.C_VALUES = pickle.load(open(c_values_file_path, "rb"))
        except :
            self.C_VALUES = self.compute_c_values(c_values_file_path)
        self.theta = theta
        self.eta = eta
        self._spatial_matrix_elements = dict()
        self.a = a
    def matrix_element(self, n_i, j1_i, m1_i, j2_i, m2_i, n_j, j1_j, m1_j, j2_j, m2_j):
        rotational = self.rotational_matrix_element(j1_i, m1_i, j2_i, m2_i, j1_j, m1_j, j2_j, m2_j)
        spatial = self.spatial_matrix_element(n_i, n_j)
        return rotational*spatial
    def C(self, j_i, m_i, j_j, m_j, p, k=1):
        sq =  sqrt((2.*j_i+1)*(2.*j_j+1)) * (-1)**m_i
        wigners = wigner_3j(j_i,k,j_j,-m_i,p,m_j)*wigner_3j(j_i,k,j_j,0,0,0)
        return sq * wigners
    def compute_c_values(self, c_values_file_path):
        two_mol_rotational_states = set()
        for _, j1, m1, _, _ in self.basis_set:
            for _, j2, m2, _, _ in self.basis_set:
                two_mol_rotational_states.add((j1, m1, j2, m2))
        C_VALUES = dict()
        for p in 0, 1, -1:
            for state in two_mol_rotational_states:
                C_VALUES[(p, state)] = self.C(*state, p)
        pickle.dump(C_VALUES, open(c_values_file_path, "wb"))
        return C_VALUES
    def spatial_matrix_element(self, n_i, n_j):
        if (n_i, n_j) in self._spatial_matrix_elements.keys():
            return self._spatial_matrix_elements[(n_i, n_j)]
        elif (n_j, n_i) in self._spatial_matrix_elements.keys():
            return self._spatial_matrix_elements[(n_j, n_i)]
        else:
            A = np.sqrt(np.pi) * gmpy2.isqrt(pow(2, n_i+n_j)) * gmpy2.isqrt(factorial(n_i)) * gmpy2.isqrt(factorial(n_j))
            dirac_delta_integral = -8/3 * 1/A * self.eta**(-1/4) * eval_hermite(n_i,-self.a) * eval_hermite(n_j,-self.a)*exp(-self.a**2) 
            numerical_integral = self.VDD_INTEGRALS[n_i,n_j]
            Udd = -1/8 * self.eta**(3/4) * (1.+3.*np.cos(2.*self.theta)) 
            self._spatial_matrix_elements[(n_i, n_j)] =  Udd * (dirac_delta_integral + numerical_integral)
            return self._spatial_matrix_elements[(n_i, n_j)]
    def rotational_matrix_element(self, j1_i, m1_i, j2_i, m2_i, j1_j, m1_j, j2_j, m2_j):
        value = self.C_VALUES[(0, (j1_i, m1_i, j1_j, m1_j))] * self.C_VALUES[(0, (j2_i, m2_i, j2_j, m2_j))]
        value += 0.5*self.C_VALUES[(1, (j1_i, m1_i, j1_j, m1_j))] * self.C_VALUES[(-1, (j2_i, m2_i, j2_j, m2_j))]
        value += 0.5*self.C_VALUES[(-1, (j1_i, m1_i, j1_j, m1_j))] * self.C_VALUES[(1, (j2_i, m2_i, j2_j, m2_j))]
        return value
    
    @property
    def unit(self):
        return "E_dd"
